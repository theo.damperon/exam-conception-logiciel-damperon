from fastapi import FastAPI
from pydantic import BaseModel
import requests

app=FastAPI()

var_id = ""

class Data(BaseModel):
    nombre_cartes:int

@app.get("/")
def racine():
    return {"Bonjour!"}

@app.get("/creer-un-deck")
def creerdeck():
    global var_id
    
    req = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/")
    data = req.json()


    var_id = data["deck_id"]
    return {"deck_id":var_id}

@app.post("/cartes")
def tirercartes(da:Data):
    global var_id
    
    if var_id == "":
        creerdeck()

    req = requests.get("https://deckofcardsapi.com/api/deck/"+var_id+"/draw/?count="+str(da.nombre_cartes))
    json = req.json()

    return {"deck_id": var_id, "cards": json["cards"]}
